// ==UserScript==
// @name             Thanopoulos batch add
// @match            https://www.thanopoulos.gr/*
// @version          1.9
// @updateURL        https://gitlab.com/astampoulis/various-userscripts/-/raw/master/greek-groceries/thanopoulos.user.js
// @require          https://code.jquery.com/jquery-3.6.0.min.js
// @grant            GM_addElement
// ==/UserScript==

$(document).ready(function() {

    $(".footer-bottom .footer-block").append($(`
        <div>
        <button id='batch-add-button' style='width: 100%; height: 30px; margin-bottom: 10px; border-radius: 5px; border: 3px solid black; box-shadow: 2px 2px 10px white;' onClick='batchAdd()'>Batch add products from textarea (code and quantity):</button>
        <textarea id='batch-products' style='width:100%; height: 50vh;'></textarea>
        <button style='width: 100%; height: 30px; margin-top: 10px; border-radius: 5px; border: 3px solid black; box-shadow: 2px 2px 10px white;' onClick='extractData()'>Copy product data to clipboard (or Ctrl-C)</button>
        </div>`));

    $(document).on('copy', evt => {
        if (!(window.getSelection().toString() === "")) return true;
        if ($("input,textarea").is(":focus")) return true;
        console.log("no selection");
        extractData();
        return false;
    });

});

async function batchAdd() {
  const input = $("#batch-products").val();

  $("#batch-add-button").prop("disabled", true);
  const items = input
    .split("\n")
    .map(line => line.split("\t"))
    .filter(([id, quantity]) => quantity && quantity != "" && !isNaN(parseInt(quantity)));
  await items.reduce(async (previous, [id, quantity], idx) => {
      await previous;
      const pct = 100.0 / items.length * idx;
      $("#batch-add-button").css("background", `linear-gradient(90deg, limegreen ${pct}%, white ${pct+1}%`);
      console.log(`adding ${id} at ${quantity}x`);
      await fetch(`https://www.thanopoulos.gr/?rand=${new Date().getTime()}`, {
          "credentials": "include",
          "headers": {
              "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0",
              "Accept": "application/json, text/javascript, */*; q=0.01",
              "Accept-Language": "en-US,en;q=0.5",
              "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
              "cache-control": "no-cache",
              "X-Requested-With": "XMLHttpRequest",
              "Sec-Fetch-Dest": "empty",
              "Sec-Fetch-Mode": "cors",
              "Sec-Fetch-Site": "same-origin",
              "Pragma": "no-cache"
          },
          "body": `controller=cart&add=1&ajax=true&pending=0&qty=${quantity}&id_product=${id}&token=${window.static_token}&id_customization=0&back=${encodeURIComponent(window.top.location.href)}`,
          "method": "POST",
          "mode": "cors"
      });
      console.log(`done with ${id} at ${quantity}x`);
  }, Promise.resolve());
  location.reload();
}

async function extractData() {

  let result = [];

  if ($("body").attr("id") === "product") {
    // individual product
    const productName = $(".product-element *[itemprop='name']").text().trim();
    const productId = $(".product-element").attr("data-id-product");
    const productQuantity = $(".product-element input#quantity_wanted").val();
    result.push({productName, productId, productQuantity});
  } else if ($("body").attr("id") === "cart") {
    // cart
    $(".cart_item").each((i, item) => {
        const productName = $(item).find(".product-name").text().trim();
        const productId = $(item).attr("data-id-product");
        const productQuantity = $(item).find(".cart_quantity_button > input[type='hidden']:first").val();
        result.push({productName, productId, productQuantity});
    });
  } else if ($("body").attr("id") === "module-blockshoppinglist-view") {
    // shopping list
    $(".list_item").each((i, item) => {
        const productName = $(item).find(".desktop-name").text().trim();
        const productId = $(item).attr("data-id-product");
        const productQuantity = $(item).find(".shopping_list_quantity_input").val();
        result.push({productName, productId, productQuantity});
    });
  } else {
    alert(`Cannot handle page type of ${$("body").attr("id")}`);
    return;
  }

  const tsv = result
    .map(({productName, productId, productQuantity}) =>
      `${productName}\t${productId}\t${productQuantity}`
    )
    .join("\n");
  await navigator.clipboard.writeText(tsv);
  alert(`Products copied to clipboard`);

}

GM_addElement('script', { textContent: batchAdd.toString() });
GM_addElement('script', { textContent: extractData.toString() });
