// ==UserScript==
// @name             AB batch add
// @match            https://www.ab.gr/*
// @version          1.9
// @updateURL        https://gitlab.com/astampoulis/various-userscripts/-/raw/master/greek-groceries/ab.user.js
// @require          https://code.jquery.com/jquery-3.6.0.min.js
// @grant            GM_addElement
// ==/UserScript==

$(document).ready(function() {
    $("footer > div").append($(`
        <div>
        <button id='batch-add-button' style='width: 100%; height: 30px; margin-bottom: 10px; border-radius: 5px; border: 3px solid black; box-shadow: 2px 2px 10px white;' onClick='batchAdd()'>Batch add products from textarea (code and quantity):</button>
        <textarea id='batch-products' style='width:100%; height: 50vh; background: rgb(37, 35, 44);'></textarea>
        <button style='width: 100%; height: 30px; margin-top: 10px; border-radius: 5px; border: 3px solid black; box-shadow: 2px 2px 10px white;' onClick='extractData()'>Copy product data to clipboard (or Ctrl-C)</button>
        </div>`));

    $(document).on('copy', evt => {
        if (!(window.getSelection().toString() === "")) return true;
        if ($("input,textarea").is(":focus")) return true;
        console.log("no selection");
        extractData();
        return false;
    });

    GM_addElement('script', { textContent: batchAdd.toString() });
    GM_addElement('script', { textContent: extractData.toString() });
});

async function batchAdd() {
  const input = document.getElementById("batch-products").value;

  $("#batch-add-button").prop("disabled", true);
  const items = input
    .split("\n")
    .map(line => line.split("\t"))
    .filter(([id, quantity]) => quantity && quantity != "" && !isNaN(parseInt(quantity)));
  await items.reduce(async (previous, [id, quantity], idx) => {
      await previous;
      const pct = 100.0 / items.length * idx;
      $("#batch-add-button").css("background", `linear-gradient(90deg, limegreen ${pct}%, white ${pct+1}%`);
      console.log(`adding ${id} at ${quantity}x`);
      await fetch("https://api.ab.gr/", {
          "credentials": "include",
          "headers": {
              "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0",
              "Accept": "*/*",
              "Accept-Language": "en-US,en;q=0.5",
              "content-type": "application/json",
              "apollographql-client-name": "gr-ab-web-stores",
              "apollographql-client-version": "029fd5ddb6129554c44c3e88f046400e5ce32dd0",
              "X-APOLLO-OPERATION-NAME": "UpdateItemQuantity",
              "X-APOLLO-OPERATION-ID": "cccfc397fbebf1a0c798523815d6f3da5f484007c4172262d968dd00911a1ec9",
              "x-default-gql-refresh-token-disabled": "true",
              "Sec-Fetch-Dest": "empty",
              "Sec-Fetch-Mode": "cors",
              "Sec-Fetch-Site": "same-site",
              "Pragma": "no-cache",
              "Cache-Control": "no-cache"
          },
          "referrer": "https://www.ab.gr/",
          "body": `{"operationName":"UpdateItemQuantity","variables":{"lang":"gr","entries":[{"productCode":"${id}","boughtPerCase":false,"quantityDelta":"${quantity}"}]},"query":"mutation UpdateItemQuantity($lang: String!, $anonymousCartCookie: String, $entries: [EntryInput], $philipsTrackingId: String) {\\n  cart: changeItemQuantity(lang: $lang, anonymousCartCookie: $anonymousCartCookie, entries: $entries, philipsTrackingId: $philipsTrackingId) {\\n    ...CartResponse\\n    __typename\\n  }\\n}\\n\\nfragment CartResponse on Cart {\\n  isNewAnonymousCart\\n  entries {\\n    product {\\n      name\\n      code\\n      productProposedPackaging\\n      manufacturerName\\n      manufacturerSubBrandName\\n      price {\\n        supplementaryPriceLabel1\\n        supplementaryPriceLabel2\\n        discountedUnitPriceFormatted\\n        __typename\\n      }\\n      url\\n      images {\\n        url\\n        format\\n        imageType\\n        altText\\n        __typename\\n      }\\n      potentialPromotions {\\n        title\\n        toDisplay\\n        redemptionLevel\\n        __typename\\n      }\\n      stock {\\n        inStock\\n        __typename\\n      }\\n      maxOrderQuantity\\n      __typename\\n    }\\n    giveAway\\n    entryNumber\\n    quantity\\n    boughtPerCase\\n    totalPrice {\\n      formattedValue\\n      variableStorePrice\\n      approximatePriceSymbol\\n      __typename\\n    }\\n    originalPrice {\\n      formattedValue\\n      __typename\\n    }\\n    showStrikethroughPrice\\n    __typename\\n  }\\n  appliedProductPromotions {\\n    consumedEntries {\\n      orderEntryNumber\\n      __typename\\n    }\\n    promotion {\\n      alternativePromotionMessage\\n      title\\n      toDisplay\\n      redemptionLevel\\n      priceToBurn\\n      simplePromotionMessage\\n      __typename\\n    }\\n    description\\n    __typename\\n  }\\n  totalUnitCount\\n  totalPrice {\\n    formattedValue\\n    approximatePriceSymbol\\n    variableStorePrice\\n    __typename\\n  }\\n  unavailableAndPartialyInStockEntries {\\n    entryNumber\\n    __typename\\n  }\\n  guid\\n  code\\n  dispatchType\\n  timeslot {\\n    slotTimeRangeFmt\\n    endTime\\n    slotCode\\n    dateFmt\\n    __typename\\n  }\\n  deliveryAddress {\\n    addressNickName\\n    houseNumberName\\n    line1\\n    addressUid\\n    formattedAddress\\n    postalCode\\n    town\\n    __typename\\n  }\\n  collectionPointId\\n  comment\\n  philipsTrackingId\\n  liquidFeeThreshold\\n  liquidFees {\\n    formattedValue\\n    __typename\\n  }\\n  __typename\\n}\\n"}`,
          "method": "POST",
          "mode": "cors"
      });
      console.log(`done with ${id} at ${quantity}x`);
  }, Promise.resolve());
  location.reload();
}

async function extractData() {
  let result = [];

  const pageSingleProduct = $(`main script[type='application/ld+json']:contains('"@type":"Product"')`).length != 0;
  const pageCart = $("div[data-testid='products-block']").length != 0;
  const pageList = $("div[data-testid='qs-my-orders-item']").length != 0;

  if (pageSingleProduct) {
      const product = JSON.parse($(`main script[type='application/ld+json']:contains('"@type":"Product"')`).text());
      const productName = `${product.brand.name} ${product.name}`
      const productId = `${product.url.replace(/^.+\//, "")}`;
      const productQuantity = "";
      result.push({productName, productId, productQuantity});
  } else if (pageCart) {
      $("div[data-testid='products-block'] div[data-testid='product-block']").each((i, item) => {
        const brandName = $(item).find("*[data-testid='product-block-brand-name']").text().trim();
        const mainName = $(item).find("*[data-testid='product-block-product-name']").text().trim();
        const productName = `${brandName} ${mainName}`;
        const productId = $(item).attr("data-test-product-id");
        const productQuantity = $(item).find("input[data-testid='product-block-quantity-input']").val();
        result.push({productName, productId, productQuantity});
      });
  } else if (pageList) {
      $("div[data-testid='qs-my-orders-item'] div[data-testid='product-block']").each((i, item) => {
        const brandName = $(item).find("*[data-testid='product-block-brand-name']").text().trim();
        const mainName = $(item).find("*[data-testid='product-block-product-name']").text().trim();
        const productName = brandName == '-' ? mainName : `${brandName} ${mainName}`;
        const productId = $(item).attr("data-test-product-id");
        const productQuantity = "";
        result.push({productName, productId, productQuantity});
      });
  } else {
    alert(`Cannot handle page type of ${$("body").attr("id")}`);
    return;
  }

  const tsv = result
    .map(({productName, productId, productQuantity}) =>
      `${productName}\t\t\t${productId}\t${productQuantity}`
    )
    .join("\n");

  await navigator.clipboard.writeText(tsv);
  alert(`Products copied to clipboard`);
}
